\documentclass[a4paper]{article}
\usepackage{graphicx}   % Para insertar gráficos
\usepackage{titling}    % Titulo y caratula
\usepackage{float}      % Para el argumento H en las figuras
\usepackage{wrapfig}    % wrapfigures
\usepackage{subcaption} % Subfigures
\usepackage{multirow, makecell}   % Filas dobles en tablas
\usepackage{wrapfig}    % Texto alrededor de figuras
\usepackage[justification=centering]{caption} % Centrar epigrafes
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{hyperref}

\graphicspath{ {graficos} } % Path por defecto para buscar graficas
\renewcommand{\contentsname}{Índice} % Cambiar nombre del tableofcontents
\renewcommand{\figurename}{Figura} % Cambiar texto en captions de figuras
\renewcommand{\tablename}{Tabla}   % Cambiar texto en captions de tabla

\pretitle{\noindent\rule{\textwidth}{2pt}\bigskip\begin{center}\LARGE\bfseries}
\posttitle{\end{center}\bigskip\noindent\rule{\textwidth}{2pt}}
\preauthor{\vspace*{2cm}\begin{center}\LARGE}
\postauthor{\end{center}}
\predate{\vspace*{10cm}\begin{center}\Large}
\postdate{\end{center}}

\title{
  Probabilidad y Estadística\\
  Trabajo Práctico - Estadística Descriptiva
}
\author{Peirone Bautista, Scola Luciano}
\date{Junio, 2023}
\begin{document}
\begin{titlepage}
\maketitle
\thispagestyle{empty} % No cuenta la portada para el numero de paginas
\end{titlepage}
\newpage

\tableofcontents
\newpage

\section{Introducción}
Una problemática recurrente en el siglo XXI es la contaminación del aire, fuente de
diversos problemas de salud como lo son cánceres, enfermedades cardiovasculares, asma,
infecciones de las vías respiratorias, etc., junto con problemas ambientales relacionados
como lo es el calentamiento global, directamente relacionado con los gases invernadero.

Todas estas problemáticas tienen un denominador común, un elemento de la naturaleza
que logra contrarrestar de forma efectiva tanto la contaminación del aire como la
generación y retención de gases invernadero. Estos son los árboles, de gran importancia
para la vida, y que cobran un rol aún más importante en las grandes ciudades,
lugares donde la contaminación del aire suele ser mayor.

Son los árboles quienes toman el rol en la naturaleza de absorber los componentes
gaseosos tóxicos, principalmente el CO2, al que transforman en oxígeno para su posterior
liberación a la atmósfera, logrando esto mediante distintos mecanismos con que
cada especie cuenta.

A partir de esto es que surge un interés por estudiar el arbolado urbano, y es él
principal motivo que moviliza la realización del Censo Forestal Urbano Público
en dos comunas al sur de Buenos Aires en el año 2011.

Las fuentes de los datos usadas en este trabajo provienen de este censo
realizado en Buenos Aires en el año 2011, y todos los análisis, gráficos y tablas
basados en datos concretos que se presentarán en este informe se basan en esta fuente.

\section{Objetivo}
El objetivo de este informe es presentar un análisis descriptivo acerca de datos
recopilados sobre el arbolado urbano en la ciudad de Buenos Aires en el año 2011,
con el fin de proveer al lector de una idea general de este y relacionarlo con la
problemática que motivó la realización del censo en primer lugar, la contaminación
del aire en la ciudad.

A lo largo del informe se presentarán tablas y gráficos para resumir la información
del censo, con el fin de entregar de manera resumida, concisa e interpretada los
datos de la fuente, los cuales por su gran volumen de datos, no son fáciles
de comprender y de rescatar conclusiones con tan solo una mirada por encima.

Para cada variable presentada se propone un contexto en como la variable afecta
a la problemática tratada, y por último una síntesis de esta en la muestra estudiada.

\newpage

\section{Desarrollo}
Los datos sobre los cuales se trabaja en este informe proveen información acerca
de distintos aspectos de una muestra tomada del arbolado urbano en Buenos Aires.
Tales aspectos son: altura, inclinación, diámetro, especie, origen y cantidad de
brotes en el último año, donde la altura está dada en metros(m), la inclinación
en grados(°) y el diámetro en centímetros(cm).

A lo largo del trabajo, nuestro objeto de interés serán árboles de la muestra,
por lo que la unidad de análisis será un árbol, y se analizarán tanto los aspectos
de los árboles por separado tanto análisis que relacionen dos características
de estos.

\subsection{Altura}
La altura es un factor clave en los árboles, principalmente en situación urbana y
es de gran importancia para la sombra que produce un árbol, donde gran cantidad de
árboles altos tienden a disminuir la temperatura de una ciudad.

Esta característica se mide en metros, aumenta de manera continua durante los años,
y va variando de una especie a otra. Además, posee un cero absoluto, esto significa
la ausencia de altura como tal, y desde luego que no son posibles valores negativos.

El siguiente histograma junto con los polígonos de frecuencia relativa
y frecuencia absoluta acumulada son de ayuda para entender como la altura se
distribuye y se establece en la muestra de estudio.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{altura/histograma.png}
  \caption{Ejemplo de histograma}
  \label{fig:histograma altura}
\end{figure}
\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{altura/poligono frecuencia.png}
    \caption{Polígono de frecuencias relativas \\ de intervalos de altura}
    \label{fig:poligono frecuencia altura}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{altura/poligono acumulativo.png}
    \caption{Polígono de frecuencias absoluta \\ acumulada de intervalos de alturas}
    \label{fig:poligono acumulada altura}
  \end{subfigure}
  \caption{Polígonos de frecuencia según intervalos de altura}
\end{figure}

\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Intervalos \\ de altura}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Frec. absoluta \\ acumulada}
    & \multirowcell{2}{Frec. relativa \\ acumulada} \\
    & & & & \\
    \hline
    $(0,5]$   &  36  & 0.10 &  36 & 0.10 \\\hline
    $(5,10]$  &  87  & 0.25 & 123 & 0.35 \\\hline
    $(10,15]$ &  85  & 0.24 & 208 & 0.59 \\\hline
    $(15,20]$ &  73  & 0.21 & 281 & 0.80 \\\hline
    $(20,25]$ &  46  & 0.13 & 327 & 0.93 \\\hline
    $(25,30]$ &  15  & 0.04 & 342 & 0.98 \\\hline
    $(30,35]$ &   6  & 0.02 & 348 & 0.99 \\\hline
    $(35,40]$ &   2  & 0.01 & 350 & 1.00 \\\hline
    Total     & 350  & 1.00 &     &     \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias de rangos de alturas}
  \label{tab:frecuencia alturas}
\end{table}

En la figura \ref{fig:histograma altura} se puede observar la distribución de
las alturas de los árboles en la muestra, donde se observa que la mayoría de
árboles se encuentran en el rango de 5 a 20 metros de altura.

Puntualmente, en el gráfico \ref{fig:poligono frecuencia altura} se observa la
frecuencia relativa de estas, y se aprecia la forma que toma la muestra, frecuencias
relativas bajas para alturas chicas y muy altas, y es mayor en las alturas en intervalos
más centrales. En concreto, se tiene una mediana de 14 metros y una desviación
standard de 7.3 metros.

Del gráfico \ref{fig:poligono acumulada altura} vemos que alrededor de 300 árboles
de la muestra tienen una altura de hasta 20 metros, indicando que solo 50 árboles
están por encima de los 20 metros. Esto es importante a tener en cuenta al hablar
de las alturas de los árboles y como se distribuyen las alturas en la muestra.

En la tabla \ref{tab:frecuencia alturas} se encuentra la frecuencia con la que
sucede cada intervalo de alturas.

Finalmente, y a modo de resumen, se encuentra un mínimo de 1 m, máximo de 38 m,
primer y segundo cuartil de 9 y 19 m respectivamente, con un rango inter cuartil
de 10, mediana de 14 m, media de 14.2 m, y finalmente varianza de 52.5 m$^2$
cuadrados y distribución standard de 7.2 m.

\subsection{Diámetro}
El diámetro, al igual que la altura, es una característica importante que crece de
manera continua y se mide en centímetros, ya que representa una magnitud física del árbol.

Se presenta el siguiente gráfico que contiene varios datos sobre el diámetro de
los árboles en la muestra.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.85\textwidth]{diametro/boxplot.png}
    \caption{Gráfico de caja de diámetros}
    \label{fig:boxplot diametro}
\end{figure}

Del gráfico \ref{fig:boxplot diametro} se pueden obtener las siguientes conclusiones:
La mediana del diámetro de los árboles en la muestra es aproximadamente 30 cm, con primer y
segundo cuartil de 20 cm y 52 cm respectivamente. Hay varios valores outliers por derecha,
y el máximo valor de diámetro que no es outlier corresponde con 100 cm, mientras que el
valor mínimo es de 1 cm. El rango intercuartil es de 32 cm, la media es de 39 cm y
la desviación standard vale 27.7 cm. La varianza tiene un valor de 767 cm$^{2}$.

La tabla \ref{tab:frecuencia diametros} presenta las datos sobre frecuencias de los diámetros
observados en intervalos de 20cm de longitud. Podemos ver de la frecuencia relativa acumulada que
los árboles con diámetros de hasta 40cm conforman el 60\% de la muestra.
\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Intervalos \\ de diámetros}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Frec. absoluta \\ acumulada}
    & \multirowcell{2}{Frec. relativa \\ acumulada} \\
    & & & & \\
    \hline
    $[0,20)$    &  84 & 0.24 &  84 & 0.24 \\\hline
    $[20,40)$   & 137 & 0.39 & 221 & 0.63 \\\hline
    $[40,60)$   &  56 & 0.16 & 277 & 0.79 \\\hline
    $[60,80)$   &  40 & 0.11 & 317 & 0.91 \\\hline
    $[80,100)$  &  20 & 0.06 & 337 & 0.96 \\\hline
    $[100,120)$ &   5 & 0.01 & 342 & 0.98 \\\hline
    $[120,140)$ &   6 & 0.02 & 348 & 0.99 \\\hline
    $[140,160)$ &   2 & 0.01 & 350 & 1.00 \\\hline
    Total     & 350  & 1.00 &     &     \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias de rangos de diámetros}
  \label{tab:frecuencia diametros}
\end{table}

\subsection{Inclinación}
La inclinación de los árboles es importante a la hora de analizar el riesgo de caída
que poseen. Esta variable depende mucho de las condiciones en la cuáles el árbol
nace, ya que un árbol que es tutelado desde su plantación presentará prácticamente
inclinación nula.

La muestra brinda medidas en grados (°), que representan el ángulo que forma el árbol
con el eje perpendicular del suelo. Sin embargo, por la naturaleza de esta y la forma
que presentan los datos, se decide clasificar la variable en dos categorías:
inclinación nula e inclinación positiva, las cuales representan el subconjunto de árboles
de la muestra con inclinación igual a cero y mayor a cero respectivamente.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.65\textwidth]{inclinacion/histograma.png}
  \caption{Histograma de densidad de inclinación}
  \label{fig:histograma inclinacion}
\end{figure}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{inclinacion/pie.png}
  \end{center}
  \caption{Gráfico de porcentajes según categoría de inclinación}
  \label{fig:barplot inclinacion}
\end{wrapfigure}

Apreciamos de la figura \ref{fig:barplot inclinacion} que gran parte de los árboles
de la muestra no están inclinados. Este es un factor muy favorable para la ciudad,
ya que cuanto mas inclinado esta un árbol, mayores son los riesgos incidentes y
la calidad de su sombra se ve reducida.

A partir de esto, planteamos un análisis únicamente sobre la inclinación de
aquellos árboles que están considerados en la categoría de inclinación positiva.

Para ello se han tomado intervalos de distintas longitudes, ya que se considera
que una inclinación entre 25° y 40° puede resultar en un peligro, inclinaciones
que varían entre los 15° y 25° son considerables pero no un posible problema, e
inclinaciones menores a esto son consideradas seguras.

En el histograma de densidades \ref{fig:histograma inclinacion} se observa que
las densidades de los intervalos de inclinación a partir de los 15° es muy baja,
indicando una frecuencia absoluta baja de árboles con gran grado de inclinación.

Sobre lo comentado con anterioridad, se aprecia de la tabla \ref{tab:frecuencia inclinacion}
que solo 7 unidades de la muestra tienen una inclinación considerable, representando
estos un 6\% del total de árboles con inclinaciones no nulas.

\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Intervalos \\ de inclinación}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Porcentaje (\%)}
    & \multirowcell{2}{Porcentaje acumulado (\%)} \\
    & & & & \\
    \hline
    $(0,5]$   & 27  &  27 &  24 &  24 \\\hline
    $(5,10]$  & 41  &  68 &  37 &  61 \\\hline
    $(10,15]$ & 21  &  89 &  19 &  80 \\\hline
    $(15,25]$ & 15  & 104 &  14 &  94 \\\hline
    $(25,40]$ &  7  & 111 &   6 & 100 \\\hline
    Total     & 111 &     & 100 &     \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias de rangos de inclinación}
  \label{tab:frecuencia inclinacion}
\end{table}

\begin{figure}[h]
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{inclinacion/poligono frecuencia.png}
    \caption{Polígono de frecuencias relativas \\ de árboles con inclinación no nula}
    \label{sfig:poligono frecuencia inclinacion}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{inclinacion/poligono acumulativo.png}
    \caption{Polígono de frecuencias absolutas acumuladas \\  de árboles con inclinación no nula}
    \label{sfig:poligono acumulativo inclinacion}
  \end{subfigure}
  \caption{Polígonos asociados a la figura \ref{fig:histograma inclinacion}}
  \label{fig:poligonos inclinacion}
\end{figure}

En el gráfico \ref{sfig:poligono acumulativo inclinacion} observamos que aproximadamente
90 árboles de aquellos en la categoría de inclinación positiva tienen una inclinación
de hasta 15°.

Finalmente, las medidas resumen de la inclinación (positiva y nula) son:
Mínimo y máximo respectivos de 0° y 40°, primer y segundo cuartil con valores de
0° y 5°, lo cual da un rango intercuartil de 5. La desviación standard es de
6.7°, mediana de 0° y media de 3.5°.

\subsection{Origen}
El origen de un árbol nos brinda información sobre sus cualidades y resistencia a
cierto tipos de ambientes. La muestra nos presenta dos posibles tipos de origen,
exótico o nativo/autóctono.

De acuerdo a la figura \ref{fig:pie origen}, la cantidad de árboles exóticos en
la muestra es superior a la de árboles nativos. Esto significa que la moda para el origen es la
categoría "exóticos".

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{origen/pie.png}
  \caption{Porcentajes de origen}
  \label{fig:pie origen}
\end{figure}

Es de gran interés de acuerdo a la problemática presentada saber si los árboles
exóticos presentan una tendencia a ser mas altos que los nativos, dando así una
razón más para el plantado de árboles exóticos.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{origen/boxplot altura.png}
  \caption{Boxplot de altura según origen}
  \label{fig:boxplot origen altura}
\end{figure}

Se observa en el gráfico \ref{fig:boxplot origen altura} que la mediana de las
alturas es mayor para los árboles exóticos, y lo mismo para el primer y tercer
cuartil entre ambas categorías. Esto indica una tendencia de mayor altura en
los árboles exóticos que en los nativos.

\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Origen}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Frec. absoluta \\ acumulada}
    & \multirowcell{2}{Frec. relativa \\ acumulada} \\
    & & & & \\
    \hline
    Exótico          & 237 & 0.68 & 237 & 0.68 \\\hline
    Nativo/Autóctono & 113 & 0.32 & 350 & 1.00 \\\hline
    Total            & 350 & 1.00 &     &     \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias por origen}
  \label{tab:frecuencia origen}
\end{table}

En la tabla \ref{tab:frecuencia origen} se aprecian las frecuencias absolutas y
relativas de cada categoría.

\subsection{Especie}
La especie de un árbol es de suma importancia a la hora de analizar la arboleda.
Esta nos representa la estructura de un árbol. Por ejemplo,
como se conforman las hojas, la estructura de su corteza, entre otras variantes.
Es claro que árboles de una misma especie entonces suelen presentar tendencias
en común, como por ejemplos alturas, diámetros, tamaños de hoja similares, entro otros.
En la muestra se presentan 9 especies distintas de árboles.

En el gráfico de barras (\ref{fig:frecuencia especie}) se presenta la frecuencia absoluta
de cada especie en la muestra, haciendo una división a su vez entre
aquellas especies que son exóticas con las autóctonas.

Como se es de esperar, por lo analizado en la sección de la variable origen, la suma de
frecuencias absolutas de las especies exóticas es mayor a la de especies autóctonas, ya que se había
concluido que el porcentaje de las primeras era mayor que el de las segundas. Se observa  además que la moda
entre las especies es el eucalipto, con aproximadamente 85 unidades en la muestra.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{especie/barplot.png}
  \caption{Gráfico de barras para la frecuencia\\absoluta de cada especie}
  \label{fig:frecuencia especie}
\end{figure}

% Para ver mas detalladamente la proporción de especies, se presentan los siguientes
% gráficos

\begin{figure}[H]
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{especie/pie exotico.png}
    \caption{Porcentajes de especies entre especies exóticas}
    \label{fig:pie exotico}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{especie/pie nativo.png}
    \caption{Porcentajes de especies entre especies nativas}
    \label{fig:pie nativo}
  \end{subfigure}
  \caption{Porcentajes de especies clasificadas según origen}
\end{figure}

En los gráficos de las figuras \ref{fig:pie exotico} y \ref{fig:pie nativo} se
observan las proporciones con que sucede cada especie entre aquellos árboles que
son exóticos y nativos respectivamente. Concretamente se observa que entre los árboles
exóticos hay una amplia variedad de especies, siendo el eucalipto aquella que domina,
y con el resto teniendo todas proporciones cercanas entre sí, mientras que entre
los árboles nativos se puede ver que son principalmente dos especies la que conforman
este grupo, el Palo Borracho y el Jacarandá, entre las cuales hacen mas de un 80\%
de la muestra de árboles nativos.

\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Especie}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Frec. absoluta \\ acumulada}
    & \multirowcell{2}{Frec. relativa \\ acumulada} \\
    & & & & \\
    \hline
    Acacia        & 19 & 0.05 &  19 & 0.05 \\\hline
    Álamo         & 48 & 0.14 &  67 & 0.19 \\\hline
    Casuarina     & 35 & 0.10 & 102 & 0.29 \\\hline
    Ceibo         & 16 & 0.05 & 118 & 0.34 \\\hline
    Eucalipto     & 83 & 0.24 & 201 & 0.57 \\\hline
    Ficus         & 12 & 0.03 & 213 & 0.61 \\\hline
    Fresno        & 43 & 0.12 & 256 & 0.73 \\\hline
    Jacarandá     & 42 & 0.12 & 298 & 0.85 \\\hline
    Palo borracho & 52 & 0.15 & 350 & 1.00 \\\hline
    Total        & 350 & 1.00 &     &      \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias por especies}
  \label{tab:frecuencia especie}
\end{table}

\subsection{Brotes}
La cantidad de brotes de un árbol es un indicador importante a la hora saber el
estado de salud del mismo, ya que, para poder producirlos, el árbol tiene que
gastar grandes cantidades de energía. Los brotes son crecimientos de hojas que se
suelen dar en los árboles, y es de gran interés ya que son las hojas de los árboles
quienes, además de producir sombra, llevan a cabo el proceso de intercambio de CO2
en el ambiente.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{brotes/bastones.png}
    \caption{Frecuencia absoluta de cantidad de brotes en el último año}
    \label{fig:bastones brotes}
\end{figure}

En el gráfico de bastones \ref{fig:bastones brotes} se observa que la cantidad de
árboles con 0 brotes es insignificante. Entre los árboles con 1 a 3 brotes hay un
rápido crecimiento lineal que nos lleva a la cantidad de brotes con mas representantes
en la muestra. Luego de la frecuencia absoluta más alta, se encuentra un decrecimiento con
forma de parábola hacia abajo hasta llegar a la máxima cantidad de brotes observada
en la muestra, que es 9.

En concreto, se cuenta con que el máximo numero de brotes alcanzado es 9, el mínimo es de 0
en aquellos que no han tenido ningún brote en el último año. El primer y segundo cuartil son
respectivamente de 3 y 5, lo que da a un rango intercuartil de 2. La mediana es 4,
la media es 3.75 aproximadamente, y el desvío standard es de 1.5.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{brotes/poligono acumulativo.png}
    \caption{Frecuencia absoluta acumulada de cantidad de brotes en el último año}
    \label{fig:poligono acumulativo brotes}
\end{figure}

Si se presta atención al gráfico \ref{fig:poligono acumulativo brotes} es posible
observar como más de 300 árboles en la muestra han tenido hasta 7 brotes en el
último año. Intentar aumentar el numero de árboles que pasan esta cantidad debe
ser un objetivo a futuro si consideramos la importancia que presenta a nuestra
problemática, lo que fomenta el cuidado y mantención en buen estado de los árboles
de la ciudad.

Los datos más en detalle se hayan en la tabla \ref{tab:frecuencia brotes} 
\begin{table}[ht]
  \centering
  \begin{tabular}{|c|r|r|r|r|}
    \hline
      \multirowcell{2}{Número de brotes}
    & \multirowcell{2}{Frecuencia \\ absoluta}
    & \multirowcell{2}{Frecuencia \\ relativa}
    & \multirowcell{2}{Frec. absoluta \\ acumulada}
    & \multirowcell{2}{Frec. relativa \\ acumulada} \\
    & & & & \\
    \hline
    0 &  1 & 0.00 &   1 & 0.00 \\\hline
    1 & 10 & 0.03 &  11 & 0.03 \\\hline
    2 & 59 & 0.17 &  70 & 0.20 \\\hline
    3 & 99 & 0.28 & 169 & 0.48 \\\hline
    4 & 84 & 0.24 & 253 & 0.72 \\\hline
    5 & 49 & 0.14 & 302 & 0.86 \\\hline
    6 & 34 & 0.10 & 336 & 0.96 \\\hline
    7 & 10 & 0.03 & 346 & 0.99 \\\hline
    8 &  3 & 0.01 & 349 & 1.00 \\\hline
    9 &  1 & 0.00 & 350 & 1.00 \\\hline
    Total   & 350 & 1.00 &     &      \\
    \hline
  \end{tabular}
  \caption{Tabla de frecuencias de número de brotes}
  \label{tab:frecuencia brotes}
\end{table}

\subsection{Especies y altura}
Relacionar las cualidades de los árboles con su especie es fundamental a la hora de
forestar zonas urbanas, ya que permite saber a priori como se va a conformar la
estructura de la arboleda que se planea implantar.

La altura, como se mencionó anteriormente, juega un rol crucial. Esto motiva a estudiar
como se relaciona cada especie con las distintos rangos de alturas.

\begin{table}[ht]
  \begin{tabular}{|c|r|r|r|r|r|r|r|r|r|}
    \hline
      \multirowcell{2}{Intervalos \\ de altura}
    & \multirowcell{2}{Acacia}
    & \multirowcell{2}{Álamo}
    & \multirowcell{2}{Casuarina}
    & \multirowcell{2}{Ceibo}
    & \multirowcell{2}{Eucalipto}
    & \multirowcell{2}{Ficus}
    & \multirowcell{2}{Fresno}
    & \multirowcell{2}{Jacarandá}
    & \multirowcell{2}{Palo \\ borracho} \\
    & & & & & & & & &\\\hline
    $[0,  5)$ & 0.105 & 0.042 & 0.029 & 0.250 & 0.012 & 0.333 & 0.070 & 0.119 & 0.096 \\\hline
    $[5, 10)$ & 0.474 & 0.250 & 0.143 & 0.312 & 0.024 & 0.250 & 0.256 & 0.262 & 0.269 \\\hline
    $[10,15)$ & 0.158 & 0.271 & 0.400 & 0.188 & 0.096 & 0.084 & 0.442 & 0.238 & 0.269 \\\hline
    $[15,20)$ & 0.263 & 0.229 & 0.286 & 0.250 & 0.145 & 0.333 & 0.186 & 0.357 & 0.308 \\\hline
    $[20,25)$ & 0.000 & 0.146 & 0.114 & 0.000 & 0.386 & 0.000 & 0.046 & 0.000 & 0.058 \\\hline
    $[25,30)$ & 0.000 & 0.062 & 0.028 & 0.000 & 0.181 & 0.000 & 0.000 & 0.024 & 0.000 \\\hline
    $[30,35)$ & 0.000 & 0.000 & 0.000 & 0.000 & 0.120 & 0.000 & 0.000 & 0.000 & 0.000 \\\hline
    $[35,40)$ & 0.000 & 0.000 & 0.000 & 0.000 & 0.036 & 0.000 & 0.000 & 0.000 & 0.000 \\\hline
    Total     & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 & 1.000 \\\hline
  \end{tabular}
  \caption{Tabla de frecuencias absolutas de altura según especie}
  \label{tab:frecuencia altura especie}
\end{table}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{bivariado/especie-altura.png}
  \caption{Boxplot de alturas según especies}
  \label{fig:boxplot alturas especie}
\end{figure}

En el gráfico \ref{tab:frecuencia altura especie} se ve que de entre las especies
mas altas tenemos al Eucalipto, donde casi un setenta porciento de su población se
encuentra entro los quince y treinta metros, también tiene casos en los que alcanza
hasta mas de 35 metros. La Acacia es una especie que se caracteriza por tener baja
altura, mas del setenta porciento de su población se encuentra en el rango de 0 a
15 metros. Aunque este porcentaje es bastante alto, se debe tener en cuenta que
la frecuencia absoluta de la Acacia en la muestra es bastante mas baja que los demás.
El caso contrario sucede con el Eucalipto que, como se ve en el análisis de especies,
esta es la que mas abunda de todas. En general se encuentra que la mayoría de
las especies está en un rango de entre 10 a 25 metros. Se puede observar que en
promedio, el Jacarandá y el Palo Borracho tienen alturas similares, mientras que
el Eucalipto y el Ficus son las mas dispares.

Todos los datos plasmados en la tabla anterior pueden corroborarse con el gráfico
de caja \ref{fig:boxplot alturas especie}.

\section{Conclusión}

En la muestra se obtuvieron los siguientes resultados. La altura de los árboles se
encuentra aproximadamente entre el rango de 10 y 19 metros, con una media de 14,
2 metros. Los diámetros mayoritariamente oscilan entre los 20 y 52 cm, con un
promedio de 39 cm. En términos de inclinación, apenas podemos presenciarla en
algunas unidades de análisis, donde el promedio es de 3,5° llegando a encontrar
árboles con 40° de inclinación. Dentro de las especies estudiadas, observamos que
cada una de estas puede tener tanto origen autóctono como exóticos, donde especies
como Eucalipto y el Palo Borracho domina estas categorías respectivamente, siendo
el eucalipto la más frecuente sobre toda las especies. Los árboles suelen presenciar
entre 3 a 5 brotes.

Finalmente con respecto a la relación altura-especie, se observó que el
Eucalipto, además de ser la especie más abundante, es la más alta y, por el contrario,
la Acacia es la más baja de las especies y presenta poca frecuencia. Por lo que,
en la muestra, podemos concluir que las especies más altas son las más
abundantes y las más bajas son la minoría.

\end{document}
